#!/bin/bash

ANSIBLECAP_PATH="/var/lib/ansible/local"
GIT_REPO_URL="https://gitlab.com/bibliosansfrontieres/olip/olip-post-install.git"
ANSIBLE_BIN="/usr/bin/ansible-pull"
BRANCH="master"

cd $ANSIBLECAP_PATH

echo "$ANSIBLE_BIN --purge -C $BRANCH -d $ANSIBLECAP_PATH -i hosts -U $GIT_REPO_URL main.yml >> /var/lib/ansible/ansible-pull-cmd-line.sh
echo -e "\n[+] Start configuration...follow logs : tail -f /var/log/ansible-pull.log"

$ANSIBLE_BIN --purge -C $BRANCH -d $ANSIBLECAP_PATH -i hosts -U $GIT_REPO_URL main.yml
